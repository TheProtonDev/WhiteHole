import asyncio
import os
import colorama
from github import Github
import classes.configlib
from nextcord.ext import tasks, commands
import nextcord
from nextcord.ext import commands
from itertools import cycle
from dislash import InteractionClient, Option, OptionType, OptionParam, SlashInteraction

# Static Variables
STABLE_MODE = True
statuses = cycle(
    ["/help for commands", "Made By Proton#2573"])  # Variable set with a list cycling through both statuses
token = os.getenv("BOT_TOKEN")  # Set a variable to the value of a os environment variable named BOT_TOKEN
github_token = os.getenv("GITHUB_TOKEN")  # Set a variable to the value of a os environment variable named GITHUB_TOKEN

g = Github(github_token)  # Initialize a instance of GitHub
intents = nextcord.Intents().all()  # Create an intents instance with all permissions to have no restrictions
bot_color = nextcord.Color.purple()  # Define the embed color as white
bot_error_color = nextcord.Color.red()  # Set the bot_error_color variable to Red
bot = commands.Bot(command_prefix="/",
                   intents=intents)  # Define the command prefix as / and use the already created instance of intents
inter_client = InteractionClient(bot, test_guilds=[892946438261198888, 892932084727939092, 757831965616504834])
bot.owner_id = 399451359037882368  # Set the owner to myself, Proton#2573
bot.remove_command('help')  # Remove the default help command to replace it with a better embed version later on


@bot.event
async def on_ready():  # The code inside this method will occur when the bot is connected and ready
    switch_status.start()  # This will start the status switching task
    print("WhiteHole Online")  # Print that the bot is online when ready


if STABLE_MODE:
    @bot.event
    async def on_slash_command_error(ctx, error):
        if isinstance(error, commands.CommandNotFound):
            errorcmd = error.args
            fix = errorcmd[0]
            print(fix)
            a = fix.replace('Command "', '')
            b = a.replace('" is not found', '')
            embed = nextcord.Embed(colour=bot_error_color, title="An Error Occured In The Command",
                                   description=f'```{b}``` is not a valid command!, try using /help to get the ones available to you.')
            await ctx.send(embed=embed)
        else:
            embed = nextcord.Embed(colour=bot_error_color, title="An Error Occured In The Command",
                                   description="Please submit a bug report using /reportbug")
            embed.add_field(inline=True, name="Exception:",
                            value=error)
            await ctx.send(embed=embed)
            print(colorama.Fore.RED + str(error))


@tasks.loop(seconds=5)
async def switch_status():  # Make a task to run every 5 seconds named switch_status
    await bot.change_presence(activity=nextcord.Activity(
        type=nextcord.ActivityType.playing, name=next(statuses)))  # Cycle between the two statuses in the statuses list


@inter_client.slash_command(
    name="help",
    description="Sends The Bot Help Menu"
)
async def help(ctx):  # Create a help command for the bot
    if isinstance(ctx.channel, nextcord.channel.DMChannel):  # If command executed in DM
        # Insert non admin user help command here when created
        pass
    elif not isinstance(ctx.channel, nextcord.channel.DMChannel):  # If command was not executed in a DM
        # If The User Is Staff Of The Server Or Just Have Admin Powers Then Add In The Mod Commands
        if ctx.author.guild_permissions.administrator:
            embed = nextcord.Embed(colour=bot_color, title="Help:",
                                   description="**WhiteHole has it's very own site! You can find commands [there](https://theprotondev.github.io/WhiteHole/commands.html)**")
            embed.add_field(inline=True, name="Author Proton#2573",
                            value='Use The Built In Bug Report System To Report Bugs: ```/reportbug bugreport```')
            await ctx.send(embed=embed)


@inter_client.slash_command(
    name="reportbug",
    description="Allows you to submit a bug report",
    options=[Option("bugreport", "Details of the bug that you want to report", OptionType.STRING, required=True)]
)
async def reportbug(ctx, bugreport: OptionType.STRING):  # Bug report command
    """

    :param bugreport: Bug that the user submits
    :return: Sends the submitted bug to Proton#2573 to be verified and sent to the Github repos issue section, if it can't be verified then it is added to the queue to be verified later on
    """
    repo = g.get_repo("TheProtonDev/WhiteHole")  # Get the GitHub repository for Whitehole
    if bugreport:  # Check if the user actually input a bug to report
        my_user_object = bot.get_user(bot.owner_id)  # Get my user object from fetching my user id
        embed = nextcord.Embed(colour=bot_color, title="A Bug Has Been Reported!",
                               description=f"```{bugreport}```")  # Create an embed including the bug report
        message = await my_user_object.send(content=my_user_object.mention,
                                            embed=embed)  # Send the bug report embed and mention my user to get my attention
        await message.add_reaction(emoji="✅")  # Adds a check mark reaction to the message
        await message.add_reaction(emoji="❌")  # Adds a :x: reaction to the message

        def check(reaction, user):  # Function called when reactions are added
            if user == bot.get_user(bot.owner_id) and str(reaction.emoji) == '✅':  # Checks if ✔
                embed = nextcord.Embed(colour=nextcord.Color.green(),
                                       title=f"Bug Report: #{repo.open_issues_count + 1}")
                embed.add_field(inline=True, name="Status:", value="Accepted And Submitted ✅")
                bot.loop.create_task(bot.get_channel(ctx.channel.id).send(embed=embed))
                repo.create_issue(title="Verified Automatic Bug System Bug:", body=bugreport)
                return True
            elif user == bot.get_user(bot.owner_id) and str(reaction.emoji) == "❌":  # Checks if :x:
                embed = nextcord.Embed(colour=bot_error_color, title=f"Bug Report: #{repo.open_issues_count + 1}")
                embed.add_field(inline=True, name="Status:", value="Denied ❌")
                bot.loop.create_task(bot.get_channel(ctx.channel.id).send(embed=embed))
                return True

        try:
            reaction, user = await bot.wait_for('reaction_add', timeout=7.0, check=check)
        except asyncio.TimeoutError:
            embed = nextcord.Embed(colour=bot_error_color, title=f"Bug Report: #{repo.open_issues_count + 1}")
            embed.add_field(inline=True, name="Status:",
                            value="Denied ❌ ; This Might Just Be Because The Bug Could Not Be Verified By Proton#2573 At The Time, You Will Be DMed If This Changes")
            await ctx.send(embed=embed)
            # Queue up the bug in the bug report folder to be verified later:
            queue_num = 1
            for i in os.listdir("queued_bug_reports"):
                queue_num = + 1

            queued_bug = open(f"queued_bug_reports/{queue_num}.txt",
                              "w")  # Create new file for the current bug report being queued
            queued_bug.write(bugreport)
            queued_bug.close()
            queued_bug_author = open(f"queued_bug_reports/{queue_num}={ctx.author.id}.txt",
                                     "w")  # Create new file for the current bug report's author
            queued_bug_author.close()
    else:
        embed = nextcord.Embed(colour=bot_error_color, title="Error In Command:",
                               description="You did not include the bug report argument")
        await ctx.send(embed=embed)


@inter_client.slash_command(
    name="queuedbugs",
    description="Lists the queued bugs"
)
async def queuedbugs(ctx):
    if ctx.author.id == bot.owner_id:
        embed = nextcord.Embed(colour=bot_color, title="Queued Bugs")
        author_files = []
        listed_bugs = os.listdir("queued_bug_reports")
        # List the queued bugs
        alphabet_for_assigning_bug_cases = ['🇦', '🇧', '🇨', '🇩', '🇪', '🇫', '🇬', '🇭', '🇮', '🇯', '🇰', 'l',
                                            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
        counter = 0
        for bug in listed_bugs:
            if bug.find(
                    '=') != -1:  # If the current bug file is just the author file of a queued bug case do the following:
                listed_bugs.pop(listed_bugs.index(bug))
                author_files.append(bug)
            else:
                bug_case_num = bug.replace('.txt', '')
                remove_author_file = bug.replace('', '')
                bug_report_contents = open(f"queued_bug_reports/{bug}", 'r').read()
                embed.add_field(inline=True, name=f"Queued Bug: {alphabet_for_assigning_bug_cases[counter]}",
                                value=f"{bug_report_contents}")
            counter = + 1

        message = await ctx.send(embed=embed)
        for i in range(0, counter):
            await message.add_reaction(alphabet_for_assigning_bug_cases[i])

        def check(reaction, user):  # Function called when reactions are added
            if user == bot.get_user(bot.owner_id):
                if reaction.emoji in alphabet_for_assigning_bug_cases:
                    repo = g.get_repo("TheProtonDev/WhiteHole")
                    bug_instance = int(alphabet_for_assigning_bug_cases.index(
                        reaction.emoji)) + 1  # Get the the bug case number so we know what file to open
                    bug_file = open(f"queued_bug_reports/{bug_instance}.txt", 'r+')
                    bug_report = bug_file.read()
                    repo.create_issue(title="Verified Automatic Bug System Bug:", body=f"{bug_report}")
                    for author in author_files:
                        if f"{bug_instance}=" in author:
                            author = author.replace("1=", "").replace(".txt",
                                                                      "")  # Remove unneeded parts of the file name to just leave behind the author name
                            author_username = bot.get_user(int(author)).display_name + "#" + bot.get_user(
                                int(author)).discriminator

                    # DM The Author letting them know their bug has been reported
                    try:
                        embed = nextcord.Embed(colour=bot_color, title=f"Hello {author_username}!",
                                               description=f"Your Bug Submission\n```{bug_report}```\nHas been verified by Proton#2573 And Has Been Added To [Github](https://github.com/TheProtonDev/WhiteHole/issues)")
                        bot.loop.create_task(bot.get_user(int(author)).send(embed=embed))
                    except Exception as e:
                        embed = nextcord.Embed(colour=bot_color,
                                               title=f"{author_username} Could Not Be DMed, Error Info Below:",
                                               description=f"```{e}```")
                        bot.loop.create_task(bot.get_user(bot.owner_id).send(embed=embed))
                    os.remove(f"queued_bug_reports/{bug_instance}.txt")  # Remove the bug report file
                    os.remove(
                        f"queued_bug_reports/{bug_instance}={author}.txt")  # Remove the bug report author file
                    embed = nextcord.Embed(colour=bot_color,
                                           title=f"Queued Bug #{bug_instance} Has Been Removed From Queue",
                                           description=f"{author_username} has been DMed")
                    bot.loop.create_task(bot.get_user(bot.owner_id).send(embed=embed))

        try:
            reaction, user = await bot.wait_for('reaction_add', timeout=12.0, check=check)
        except asyncio.TimeoutError:
            embed = nextcord.Embed(colour=bot_error_color, title="Timed Out!",
                                   description="The Bot Is No Longer Waiting For Reactions To Be Added, Send The Previously Used Command To Get Another 12 Seconds")
            await ctx.send(embed=embed)
    else:
        embed = nextcord.Embed(colour=bot_error_color,
                               title="You are not the bot developer, so you can not use this command!")
        await ctx.send(embed=embed)


@inter_client.slash_command(
    name="sendembed",
    description="Allows you to submit a bug report",
    options=[
        Option("title", "Title for the embed you want to create", OptionType.STRING, required=True),
        Option("channel", "Mention the channel to send this embed to, defaults to current channel if non entered",
               OptionType.CHANNEL),
        Option("description", "Description for the embed you want to create", OptionType.STRING),
        Option("rgb", "RGB Colors you want to use, input like so: 255 255 255", OptionType.STRING),
        Option("poll", "Whether or not the embed is a poll, if so then reactions will be added, y or n",
               OptionType.STRING),
        Option("poll_type",
               "Type of poll you want to send, defaults to y or n poll, input looks like 1:5 use /help for more info",
               OptionType.INTEGER),
        Option("poll_options",
               "The options you want to include, only applies if using poll type 2 on /help site for usage",
               OptionType.STRING)]
)
async def sendembed(inter: SlashInteraction, title, channel: OptionType.CHANNEL = None,
                    description: OptionType.STRING = None,
                    rgb: OptionType.STRING = None, poll: OptionType.STRING = None,
                    poll_type: OptionType.INTEGER = None, poll_options: OptionType.STRING = None):  # Bug report command
    if inter.author.guild_permissions.manage_messages:
        if rgb is None:  # If the user does not submit a RGB value set it to 0, 0, 0
            r = 0
            g = 0
            b = 0
        elif rgb:
            rgb_values = str(rgb).split()
            r = int(rgb_values[0])
            g = int(rgb_values[1])
            b = int(rgb_values[2])
        color = nextcord.Color.from_rgb(r, g, b)
        embed = nextcord.Embed(colour=color, title=title)
        embed.set_footer(icon_url=inter.author.avatar.url, text=f"Sent From {inter.author}")
        if description:  # If the user input a description argument then set the embed description to it
            embed.description = description
        if poll is None:
            if channel is None:  # If no channel is input then send it to the current channel
                message = await inter.reply(embed=embed)
            else:
                message = await channel.send(embed=embed)

        # Check if the embed is a poll to see if reactions should be added
        if poll:
            if poll_type == 1:
                if channel is None:  # If no channel is input then send it to the current channel
                    message = await inter.reply(embed=embed)
                else:
                    message = await channel.send(embed=embed)
                await message.add_reaction("✅")  # Add checkmark reaction to message
                await message.add_reaction("❌")  # Add x reaction to message
            elif poll_type == 2:
                if poll_type is None:
                    embed = nextcord.Embed(colour=bot_error_color, title="You Did Not Input Any Poll Options But You Selected Multiple Option Mode, Please Retry The Command With Options Input")
                    await inter.reply(embed=embed)
                else:
                    embed = nextcord.Embed(colour=color, title=title)
                    embed.set_footer(icon_url=inter.author.avatar.url, text=f"Sent From {inter.author}")
                    if description:  # If the user input a description argument then set the embed description to it
                        embed.description = description
                    option_list = list(poll_options.split('|'))
                    max_range = len(option_list)
                    if max_range > 10:
                        embed = nextcord.Embed(colour=bot_error_color,
                                               title="Input Range Max Of 10 Exceeded! Please retry the command with 10 options or less!")
                        await inter.reply(embed=embed)
                    elif max_range <= 10:
                        emojis = ["1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣", "1️⃣0️⃣"]  # Create a list with all emojis 1 through 10 to be used for reaction purposes
                        for option in option_list: # Loop through all the options the user input, and add a field to the embed for it
                            counter = option_list.index(option)  # Get the position of the current option in the option list
                            embed.add_field(inline=True, name=option, value=emojis[counter])
                        message = await inter.reply(embed=embed)
                        for emoji_reaction_num in range(0, max_range):
                            await message.add_reaction(emojis[emoji_reaction_num])
    else:
        embed = nextcord.Embed(colour=bot_error_color, title="You Do Not Have Access To This Command")
        await inter.reply(embed=embed)


@inter_client.slash_command(
    name="purge",
    description="Allows you to purge a select amount of messages",
    options=[Option("amount", "Amount of messages to purge", OptionType.INTEGER, required=True)]
)
async def purge(inter, amount: OptionType.INTEGER):  # Bug report command
    if inter.author.guild_permissions.manage_messages:
        await inter.channel.purge(limit=amount)
        embed = nextcord.Embed(colour=bot_color, title=f"Purged {amount} Messages!")
        purge_notice = await inter.reply(embed=embed)
        await purge_notice.delete(delay=3.0)
    else:
        embed = nextcord.Embed(colour=bot_error_color, title="You Do Not Have Access To This Command")
        await inter.reply(embed=embed)


bot.run(token)  # Run the bot using the token from env variable BOT_TOKEN
